﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using HtmlToOpenXml;

namespace ConsoleApp1
{
	// ****************************
	// Markdown
	// ****************************
	/*
# Main Title

## Sub-heading

This is a sentence with **bold** and *italic*.

Here are some bullets:

- bullet 1
- bullet 2
	- bullet 2a

Here is a list:

1. List 1
2. List 2
	1. List 2a

Reference link: [Google](https://www.google.com/)
    */

	// ****************************
	// Resulting HTML (Markdig output)
	// ****************************
	/*
<h1>Main Title</h1>

<h2>Sub-heading</h2>

<p>This is a sentence with <strong>bold</strong> and <em>italic</em>.</p>

<p>Here are some bullets:</p>

<ul>
<li>bullet 1</li>
<li>bullet 2

<ul>
<li>bullet 2a</li>
</ul></li>
</ul>

<p>Here is a list:</p>

<ol>
<li>List 1</li>
<li>List 2

<ol>
<li>List 2a</li>
</ol></li>
</ol>

<p>Reference link: <a href="https://www.google.com/">Google</a></p>
    */

	// ****************************
	// Desired Open XML
	// ****************************
	/*
<Cell ss:StyleID="s74">
	<ss:Data ss:Type="String" xmlns="http://www.w3.org/TR/REC-html40"><B>
			<Font html:Color="#000000">Main Title&#10;</Font>
			<Font html:Size="18" html:Color="#000000">&#10;Sub-heading</Font>
			<Font html:Color="#000000">&#10;</Font>
		</B>
		<Font html:Size="14" html:Color="#000000">&#10;This is a sentence with&#160;</Font><B>
			<Font html:Size="14" html:Color="#000000">bold&#160;</Font>
		</B>
		<Font html:Size="14" html:Color="#000000">and&#160;</Font><I>
			<Font html:Size="14" html:Color="#000000">italic</Font>
		</I>
		<Font html:Size="14" html:Color="#000000">.&#10;&#10;Here are some bullets:&#10;&#10;• Bullet 1&#10;• Bullet
			2&#10;• Bullet 2a</Font><B>
			<Font html:Size="14" html:Color="#000000">&#10;&#10;</Font>
		</B>
		<Font html:Size="14" html:Color="#000000">Here is a list:&#10;&#10;1. Item 1&#10;2. Item 2&#10; 1. Item
			2a&#10;&#10;Reference link: [Google](https://www.google.com/)</Font>
		<Font html:Size="12" html:Color="#000000">&#10;&#10;</Font>
		<Font html:Color="#000000">&#10;</Font><B>
			<Font html:Color="#000000">&#10;</Font>
		</B>
	</ss:Data>
</Cell>
    */

	class Program
	{
		static Dictionary<string, string> headerSize = new Dictionary<string, string> { { "1", "16" }, { "2", "14" }, { "3", "13" }, { "4", "12" } };

		/// <summary>
		/// The goal here is to do a standard HTML conversion first,
		/// then adjust the HTML as needed.
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			var html = @"
<h1>title</h1>
<h2>heading</h2>
<p>
   <strong>bold</strong>
   <em>italic</em>
</p>
<ul>
   <li>bullet 1</li>
   <li>
      bullet 2
      <ul>
         <li>bullet 2a</li>
      </ul>
   </li>
</ul>
<ol>
   <li>List 1</li>
   <li>
      List 2
      <ol>
         <li>List 2a
            <a href=""https://www.google.com/"">Google</a>
         </li>
      </ol>
   </li>
</ol>
";

			var doc = new HtmlToOpenXmlDocument(html);
			Console.WriteLine(doc.ToOpenXmlFragment());

			Console.ReadKey();

		}
	}
}
