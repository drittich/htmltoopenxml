﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;

namespace HtmlToOpenXml
{
	public class HtmlToOpenXmlDocument : HtmlDocument
	{
		public FileFormat FileFormat { get; set; }
		public Dictionary<string, string> HeaderFontPixelSizes { get; set; }

		public HtmlToOpenXmlDocument(string html)
		{
			OptionOutputUpperCase = true;

			HeaderFontPixelSizes = new Dictionary<string, string> { { "1", "16" }, { "2", "14" }, { "3", "13" }, { "4", "12" } };
			FileFormat = FileFormat;

			LoadHtml(html);
		}

		public string ToOpenXmlFragment(FileFormat fileFormat = FileFormat.Spreadsheet)
		{
			convertParagraphs();
			convertBold();
			convertItalic();
			convertHeaders();
			//convertOrderedLists(); 
			//convertUnorderedLists();

			var xmlRaw = DocumentNode.OuterHtml;
			var wrappedXml = wrapOpenXml(xmlRaw);
			return wrappedXml;
		}

		private void convertUnorderedLists()
		{
			// TODO: need a recursive function here. Format with bullets, spaces, and newlines
			throw new NotImplementedException();
		}

		private void convertOrderedLists()
		{
			// TODO: need a recursive function here. Format with numbers, spaces, and newlines
			throw new NotImplementedException();
		}

		private void convertItalic()
		{
			ReplaceAllTags("em", "I");
		}

		private void convertBold()
		{
			ReplaceAllTags("strong", "B");

		}

		// converts paragraphs to line feeds
		private void convertParagraphs()
		{
			var xpath = "//*[self::p]";
			foreach (var node in DocumentNode.SelectNodes(xpath))
			{
				AppendNewLine(node);
				RemoveNode(node);
			}
		}

		static string wrapOpenXml(string xml, string cellStyleID = "s64")
		{
			return $@"
<Cell ss:StyleID=""{cellStyleID}"">
	<ss:Data ss:Type=""String"" xmlns=""http://www.w3.org/TR/REC-html40"">
{xml}
	</ss:Data>
</Cell>
";
		}

		// Find headers and convert to bold/font tags, e.g.,
		// <B><Font html:Color="#000000">My Title</Font></B>
		void convertHeaders()
		{
			var xpath = "//*[self::h1 or self::h2 or self::h3 or self::h4]";
			foreach (var node in DocumentNode.SelectNodes(xpath))
			{
				var headingLevel = node.Name.Substring(1);
				node.Name = "B";
				var text = node.InnerText;
				node.InnerHtml = getFontNode(text, null, HeaderFontPixelSizes[headingLevel]);
			}
		}

		static string getFontNode(string text, string color = "#000000", string size = null)
		{
			return $@"<Font html:Size=""{size}"" html:Color=""{color}"">{text}</Font>" + getNewLineMarkup();
		}

		static string getNewLineMarkup(string color = "#000000")
		{
			return $"\n<Font html:Size=\"11\" html:Color=\"{color}\">&#10;</Font>";
		}
		static HtmlNode getNewLineNode(string color = "#000000")
		{
			return HtmlNode.CreateNode(getNewLineMarkup(color));
		}

		static void RemoveNode(HtmlNode node, bool keepGrandChildren = true)
		{
			node.ParentNode.RemoveChild(node, keepGrandChildren);
		}

		static void AppendNewLine(HtmlNode node)
		{
			node.ParentNode.InsertAfter(getNewLineNode(), node);
		}

		void ReplaceAllTags(string fromTag, string toTag)
		{
			var xpath = $"//*[self::{fromTag}]";
			foreach (var node in DocumentNode.SelectNodes(xpath))
			{
				node.Name = toTag;
			}
		}
	}

	// We only support Spreadsheet for now, but planned ahead so enum numbers don't change later.
	public enum FileFormat
	{
		//Presentation = 1,
		Spreadsheet = 2//,
	   //Wordprocessing = 3
	}
}
